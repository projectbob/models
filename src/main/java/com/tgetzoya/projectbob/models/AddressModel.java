/* *****************************************************************************
 * *****************************************************************************
 * *****************************************************************************
 * AddressModel.java
 * (c) 2016 Thomas Getzoyan <tgetzoya@gmail.com>
 * All Rights Reserved
 * *****************************************************************************
 * *****************************************************************************
 * ****************************************************************************/
package com.tgetzoya.projectbob.models;

import lombok.Data;

/**
 * Class for address storage.  This is for the US style.
 *
 * @author Thomas Getzoyan <tgetzoya@gmail.com>
 * @version 1.0
 * @since 1.0
 */
@Data
public class AddressModel {
    /**
     * The recipient, typically the user.
     */
    private String addressee;
    /**
     * AddressModel line 1. Ex: 123 Main St
     */
    private String line1;
    /**
     * AddressModel Line 2. Ex: Apt 7
     */
    private String line2;
    /**
     * City. Ex: Johnsonville
     */
    private String city;
    /**
     * Sate: Ex: Kansas or KS
     */
    private String state;
    /**
     * Zip or Zip+4. Ex: 91326 or 91326-4520
     */
    private String zip;
}
