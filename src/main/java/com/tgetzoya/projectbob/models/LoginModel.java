/* *****************************************************************************
 * *****************************************************************************
 * *****************************************************************************
 * LoginModel.java
 * (c) 2016 Thomas Getzoyan <tgetzoya@gmail.com>
 * All Rights Reserved
 * *****************************************************************************
 * *****************************************************************************
 * ****************************************************************************/
package com.tgetzoya.projectbob.models;

import lombok.Data;

import java.nio.ByteBuffer;
import java.util.UUID;

/**
 * Class that contains all the data required to log in (or out) a user.
 *
 * @author Thomas Getzoyan <tgetzoya@gmail.com>
 * @version 1.0
 * @since 1.0
 */
@Data
public class LoginModel {
    /**
     * User id (email address)
     */
    private String email;
    /**
     * User password
     */
    private ByteBuffer password;
    /**
     * Auth Token (for logging out)
     */
    private String authToken;
    /**
     * Internal ID
     */
    private UUID id;
}
