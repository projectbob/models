/* *****************************************************************************
 * *****************************************************************************
 * *****************************************************************************
 * NameModel.java
 * (c) 2016 Thomas Getzoyan <tgetzoya@gmail.com>
 * All Rights Reserved
 * *****************************************************************************
 * *****************************************************************************
 * ****************************************************************************/
package com.tgetzoya.projectbob.models;

import lombok.Data;

/**
 * Class for names.
 *
 * @author Thomas Getzoyan <tgetzoya@gmail.com>
 * @version 1.0
 * @since 1.0
 */
@Data
public class NameModel {
    /**
     * Title:  Ex:  Mr. or Dr. or Reverend
     */
    private String prefix;
    /**
     * First name.  Ex:  Thomas
     */
    private String first;
    /**
     * Middle name.  Ex:  Bob
     */
    private String middle;
    /**
     * Last name:  Ex.  Getzoyan
     */
    private String last;
    /**
     * Suffix.  Ex:  Esq, DVM, MBA
     */
    private String suffix;
}
