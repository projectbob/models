/* *****************************************************************************
 * *****************************************************************************
 * *****************************************************************************
 * RegisterModel.java
 * (c) 2016 Thomas Getzoyan <tgetzoya@gmail.com>
 * All Rights Reserved
 * *****************************************************************************
 * *****************************************************************************
 * ****************************************************************************/
package com.tgetzoya.projectbob.models;

import lombok.Data;

/**
 * Class that contains all the data required to register a user.
 *
 * @author Thomas Getzoyan <tgetzoya@gmail.com>
 * @version 1.0
 * @since 1.0
 */
@Data
public class RegisterModel {
    /**
     * The email address of the new user.
     */
    private String email;
    /**
     * The password for the new user.
     */
    private String password;
    /**
     * The name of the new user.  Optional, but will need to pass a validation
     * check if defined/instantiated.
     */
    private NameModel name;
    /**
     * The address of the new user.  Optional.
     */
    private AddressModel address;
}
