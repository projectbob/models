/* *****************************************************************************
 * *****************************************************************************
 * *****************************************************************************
 * ResponseModel.java
 * (c) 2016 Thomas Getzoyan <tgetzoya@gmail.com>
 * All Rights Reserved
 * *****************************************************************************
 * *****************************************************************************
 * ****************************************************************************/
package com.tgetzoya.projectbob.models;

import com.tgetzoya.projectbob.models.error.ErrorModel;
import lombok.Data;

import java.util.List;

/**
 * The standard response model.
 */
@Data
public class ResponseModel<E> {
    /**
     * Result code.  0 for "OK" and not 0 for "OH CRAP!"
     */
    private int result;
    /**
     * The list of error messages, if there are any.
     */
    private List<ErrorModel> errors;
    /**
     * Any returned data.
     */
    private E responseData;
}
