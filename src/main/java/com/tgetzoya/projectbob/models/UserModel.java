/* *****************************************************************************
 * *****************************************************************************
 * *****************************************************************************
 * UserModel.java
 * (c) 2016 Thomas Getzoyan <tgetzoya@gmail.com>
 * All Rights Reserved
 * *****************************************************************************
 * *****************************************************************************
 * ****************************************************************************/
package com.tgetzoya.projectbob.models;

import lombok.Data;

import java.nio.ByteBuffer;
import java.util.UUID;

/**
 * Class that contains all the data required to register a user.
 *
 * @author Thomas Getzoyan <tgetzoya@gmail.com>
 * @version 1.0
 * @since 1.0
 */
@Data
public class UserModel {
    /**
     * Internal user ID for the user
     */
    private UUID id;
    /**
     * External ID for the user.  Typically the users' email.
     */
    private String email;
    /**
     * An address for the user.
     */
    private AddressModel address;
    /**
     * The name of the user.
     */
    private NameModel name;
    /**
     * The user password
     */
    private ByteBuffer password;
    /**
     * The salt for the password
     */
    private ByteBuffer salt;
}
