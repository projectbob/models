/* *****************************************************************************
 * *****************************************************************************
 * *****************************************************************************
 * ErrorModel.java
 * (c) 2016 Thomas Getzoyan <tgetzoya@gmail.com>
 * All Rights Reserved
 * *****************************************************************************
 * *****************************************************************************
 * ****************************************************************************/
package com.tgetzoya.projectbob.models.error;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * Class for containing a single error message.
 *
 * @author Thomas Getzoyan <tgetzoya@gmail.com>
 * @version 1.0
 * @since 1.0
 */
@Data
@AllArgsConstructor
public class ErrorModel {
    /**
     * The error message.
     */
    private String message;
}
