/* *****************************************************************************
 * *****************************************************************************
 * *****************************************************************************
 * StandardErrors.java
 * (c) 2016 Thomas Getzoyan <tgetzoya@gmail.com>
 * All Rights Reserved
 * *****************************************************************************
 * *****************************************************************************
 * ****************************************************************************/
package com.tgetzoya.projectbob.models.error;

/**
 * Standard errors to display for validation.
 */
public class StandardErrors {
    /**
     * Email is required but was missing.
     */
    public static final ErrorModel EMAIL_MISSING = new ErrorModel("Email is Missing.");
    /**
     * First name is required but missing.
     */
    public static final ErrorModel FIRST_NAME_MISSING = new ErrorModel("First Name is Missing.");
    /**
     * Last name is required but missing.
     */
    public static final ErrorModel LAST_NAME_MISSING = new ErrorModel("Last Name is Missing.");
    /**
     * Password is required but missing.
     */
    public static final ErrorModel PASSWORD_MISSING = new ErrorModel("Password is Missing.");
}
